from django.contrib.auth.hashers import make_password
from django.test import TestCase
from .models import User
import tempfile

# Create your tests here.
class TestLogin(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testusername',
            'password': make_password('secret'),
            }
        User.objects.create(**self.credentials)

    def test_login_success(self):
        response = self.client.post('/api/login', {'username': 'testusername', 'password': 'secret'})
        self.assertEqual(response.status_code, 200)
        self.assertRaises(KeyError, lambda: response.data['error_message'])
        self.assertIsNotNone(response.data['username'])

    def test_login_error(self):
        response = self.client.post('/api/login', {'username': 'testusername', 'password': 'wrong'})
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data['error_message'])


class TestSignup(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testusername',
            'password': make_password('secret'),
            }
        User.objects.create(**self.credentials)

    def test_signup_success(self):
        response = self.client.post('/api/signup', {'username': 'new_user', 'password': 'secret'})
        self.assertEqual(response.status_code, 200)
        self.assertRaises(KeyError, lambda: response.data['error_message'])
        self.assertIsNotNone(response.data['username'])

    def test_signup_error(self):
        response = self.client.post('/api/signup', {'username': 'testusername', 'password': 'secret'})
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data['error_message'])


class TestComplete(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testusername',
            'password': make_password('secret'),
            }
        User.objects.create(**self.credentials)

    def test_complete_success(self):
        data = {
            'user_id': '1',
            'first_name': 'Juan',
            'last_name': 'Dela Cruz',
            'email': 'juandelacruz@email.com',
            'date_of_birth': '1999-02-11',
            'contact_number': '09123456789',
            'profile_picture': tempfile.NamedTemporaryFile(suffix=".jpg")
            }
        response = self.client.post('/api/complete', data)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data['username'])
        self.assertIsNotNone(response.data['profile_picture'])
