from django.contrib.auth import login, logout
from django.contrib.auth.hashers import make_password
from .models import User
from .serializers import UserSerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.
@api_view(['POST'])
def rest_login(request):
    username = request.data.get('username')
    password = request.data.get('password')

    error_response = {'error_message': 'You have entered an incorrect username or password.'}
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist as exc:
        return Response(error_response)
    if user is None:
        return Response(error_response)
    else:
        authenticate = True if user.check_password(password) else False
        if authenticate:
            user_serializer = UserSerializer(user)
            login(request, user)
            return Response(user_serializer.data)
        else:
            return Response(error_response)


@api_view(['POST'])
def rest_logout(request):
    logout(request)
    return Response({}, status= status.HTTP_200_OK)


@api_view(['POST'])
def rest_signup(request):
    username = request.data.get('username')
    password = request.data.get('password')

    error_response = {'error_message': 'This username is already in use.'}
    if User.objects.filter(username=username):
        return Response(error_response)
    else:
        user = User(username=username, password=make_password(password))
        user.save()
        user_serializer = UserSerializer(user)
        return Response(user_serializer.data)


@api_view(['POST'])
def rest_complete(request):
    user_id = request.data.get('user_id')
    first_name = request.data.get('first_name')
    last_name = request.data.get('last_name')
    email = request.data.get('email')
    date_of_birth = request.data.get('date_of_birth')
    contact_number = request.data.get('contact_number')
    profile_picture = request.FILES['profile_picture']
    
    user = User.objects.get(pk=user_id)
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.date_of_birth = date_of_birth
    user.contact_number = contact_number
    user.profile_picture = profile_picture
    user.is_complete = True

    user.save()
    user_serializer = UserSerializer(user)
    return Response(user_serializer.data)