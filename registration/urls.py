from django.urls import path
from . import views

urlpatterns = [
    path('login', views.rest_login, name='rest_login'),
    path('logout', views.rest_logout, name='rest_logout'),
    path('signup', views.rest_signup, name='rest_signup'),
    path('complete', views.rest_complete, name='rest_complete'),
]
