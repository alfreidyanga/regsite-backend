from .file_methods import userprofile_upload_to, validate_file_size, validate_image_size
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    first_name = models.CharField(max_length=50, blank= True, null=True)
    last_name = models.CharField(max_length=50, blank= True, null=True)
    email = models.EmailField(blank= True, null=True)
    date_of_birth = models.DateField(blank= True, null=True)
    contact_number = models.CharField(max_length=22, blank= True, null=True)
    profile_picture = models.ImageField(upload_to=userprofile_upload_to, height_field=None, width_field=None, max_length=200, validators=[validate_file_size, validate_image_size], default=None, null=True, blank=True)

    is_complete = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.pk}'
