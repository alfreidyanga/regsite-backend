from django.contrib import admin
from .models import User

class UserAdmin(admin.ModelAdmin):
    list_display = [
        'id', 
        'username',
        'is_complete',
    ]

# Register your models here.
admin.site.register(User, UserAdmin)