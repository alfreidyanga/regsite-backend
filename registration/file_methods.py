from django.forms import ValidationError
from PIL import Image

def validate_image_size(image):
    min_height = 0
    min_width = 0

    image = Image.open(image) 

    height = image.size[1]
    width = image.size[0]

    if width < min_width or height < min_height:
        raise ValidationError("Height or Width is smaller than what is allowed (500x500)")


def validate_file_size(value):
    filesize = value.size
    
    if filesize > 10485760:
        raise ValidationError("The maximum file size that can be uploaded is 10MB")
    else:
        return value

def userprofile_upload_to(instance, filename):
    return f'media/profiles/user_{instance.id}/{filename}'
